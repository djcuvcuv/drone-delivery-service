# Drone Delivery Service Location REST API

An HTTP server which exposes an endpoint to submit, process, and store drone delivery location metadata

### API Resources

/delivery/token.json

/delivery/location.json

### Prerequisites

In order to use this application, you will need:

1. node.js version 6 or later. You can install node.js (and co-bundled npm) here:

https://nodejs.org/en/download/

2. MongoDB version 3.2 or later. You can install MongoDB here:

https://treehouse.github.io/installation-guides/mac/mongo-mac.html

### Installing and Using

1. Clone this repo to your local and cd into the project root directory:
```
git clone https://bitbucket.org/djcuvcuv/drone-delivery-service.git
cd drone-delivery-service
```

2. Update any constants in the `app.js`, `location.js`, or `token.js` headers, such as `gmapsApiKey` or `mongouri`.

3. Install the required node modules and run the server:
```
npm install
node app.js
```

4. Retrieve an access token (expires in 60 minutes):
```
curl -X POST \
  http://localhost:9000/delivery/token.json \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"secret": "foobarsecretvalue"
}'
```

Example response body:
```
{
   "__v": 0,
   "token": "4ced9b91-3b23-41b6-870c-40eabf37b54c",
   "token_type": "bearer",
   "expires_in": 3600,
   "expires_at": 1512015032,
   "_id": "5a1f76a892f94329fe1ed45c"
}
```
5. Set the bearer token in the 'authorization' header and send HTTP requests to the location endpoint:
```
curl -X POST \
  http://localhost:9000/delivery/location.json \
  -H 'authorization: Bearer 4ced9b91-3b23-41b6-870c-40eabf37b54c' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
    "ht_ft": 100,
    "addr": "1500 Orange Ave, Coronado, CA 92118"
}'
```
Example response body:
```
{
    "message": "location record was successfully written",
    "location": {
        "__v": 0,
        "input_ht_ft": 100,
        "input_addr": "1500 Orange Ave, Coronado, CA 92118",
        "cleaned_addr": "1500%20Orange%20Ave%2C%20Coronado%2C%20CA%2092118",
        "coords": "32.6815235,-117.1778507",
        "elevation": 40.2773222732544,
        "uuid": "7463cba1-50e3-4254-9334-2f2b88c6f884",
        "_id": "5a2022a06c9d282d3fef036e"
    }
}
```

## Authors

* **Chris Covney** - https://bitbucket.org/djcuvcuv

## Copyright and License
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

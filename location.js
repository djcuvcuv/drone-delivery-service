'use strict';

/**
 * Main Delivery Service Location Processes File
 */

const fs = require('fs'),
    rp = require('request-promise'),
    uuidv4 = require('uuid/v4'),
    urlencode = require('urlencode'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    gmapsApiKey = 'AIzaSyC28cxWWXfdrBb0-2s7KEfa0vGy8M4fjME',
    gmapsUrl = 'https://maps.googleapis.com/maps/api',
    ft2m = 0.3048;

/**
 * Location request template
 * @type {{ht_ft: number, addr: string}}
 */
const locationRequestTemplate = {ht_ft: 0, addr: 'foo'};

/**
 * Location schema and model definition
 * @type {{input_ht_ft: number, input_addr: string, cleaned_addr: string, coords: string, elevation: number, uuid: string}}
 */
const Location = mongoose.model('Location', new Schema({
    input_ht_ft: {type: Number},
    input_addr: {type: String},
    cleaned_addr: {type: String},
    coords: {type: String},
    elevation: {type: Number},
    uuid: {type: String}
}));

/**
 * Compares input request body with location object template
 * @param a
 * @returns {boolean}
 */
function compareKeys(a) {
    let aKeys = Object.keys(a).sort();
    let bKeys = Object.keys(locationRequestTemplate).sort();
    return JSON.stringify(aKeys) === JSON.stringify(bKeys);
}

/**
 * Location function exposed through the HTTP API interface
 * Executes location storage to local file system
 * returns HTTP 201/500 upon success/fail
 * @param req
 * @param res
 */
let location = (req, res) => {
    if (!compareKeys(req.body)) return res.status(400).json({error: 'invalid request'});

    const id = uuidv4(),
        cleanedAddr = urlencode(req.body.addr),
        getCoords = {
            url: `${gmapsUrl}/geocode/json?address=${cleanedAddr}&key=${gmapsApiKey}`,
            json: true
        };

    let loc2 = new Location({
        input_ht_ft: req.body.ht_ft,
        input_addr: req.body.addr,
        cleaned_addr: cleanedAddr,
        coords: 'baz',
        elevation: 0,
        uuid: id
    });

    /**
     * Callout to Google Coordinates API
     */
    rp(getCoords)
        .then((body) => {
            loc2.coords = `${body.results[0].geometry.location.lat},${body.results[0].geometry.location.lng}`;
            return loc2.coords;
        })
        .then((coordStr) => {
            const getElevation = {
                url: `${gmapsUrl}/elevation/json?locations=${coordStr}&key=${gmapsApiKey}`,
                json: true
            };

            /**
             * Callout to Google Elevation API
             */
            rp(getElevation)
                .then((body) => {
                    loc2.elevation = body.results[0].elevation + (loc2.input_ht_ft * ft2m);

                    /**
                     * Writes derived location object to mongodb
                     */
                    loc2.save((err, data) => {
                        if (err) {
                            console.error(`location record storage ${loc2.uuid} failed!\n`, err);
                            return res.status(500).send({message: `location record storage ${loc2.uuid} failed!`});
                        }
                        console.log('\nlocation record written to mongodb:\n', data);
                        return res.status(201).json({
                            message: 'location record was successfully written',
                            location: data
                        });
                    });

                    /**
                     * Writes derived location object to file system ./location-store/
                     */
                    // fs.writeFile(`./location-store/${loc2.uuid}.json`, JSON.stringify(loc2), (err) => {
                    //     if (err) {
                    //         console.error(`location record storage ${loc2.uuid} failed!\n`, err);
                    //         return res.status(500).send({message: `location record storage ${loc2.uuid} failed!`});
                    //     }
                    //     console.log(`\nlocation record written to file system: ./location-store/${loc2.uuid}.json`);
                    //     console.log(`$cat ./location-store/${loc2.uuid}.json\n`, loc2);
                    //     return res.status(201).json({message: 'location record was successfully written', location: loc2});
                    // });
                })
                .catch((err) => {
                    console.error(`location elevation processing ${loc2.uuid} failed!\n`, err);
                    return res.status(500).send({message: `location elevation processing ${loc2.uuid} failed!`});
                })

        })
        .catch((err) => {
            console.error(`location coordinates processing ${loc2.uuid} failed!\n`, err);
            return res.status(500).send({message: `location coordinates processing ${loc2.uuid} failed!`});
        });
};

module.exports = location;

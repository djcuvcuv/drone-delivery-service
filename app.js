'use strict';

/**
 * Express.js HTTP API Interface
 */

const app = require('express')(),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    location = require('./location'),
    token = require('./token'),
    mongouri = 'mongodb://localhost/test',
    port = 9000;

mongoose.connect(mongouri, function (err, res) {
    if (err) console.error(`ERROR connecting to: ${mongouri}.${err}`);
    else console.log(`SUCCESS connecting to: ${mongouri}`);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use((req, res, next) => {
    console.log('\n##################### INCOMING REQUEST ######################\n', req.headers, '\n');
    next();
});

app.post('/delivery/token.json', token.returnToken);
app.use(token.authorize);
app.post('/delivery/location.json', location);
app.listen(port, () => console.log(`\ntry: curl -v -X POST http://localhost:${port}/delivery/location.json\n`));

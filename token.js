'use strict';

const mongoose = require('mongoose'),
    uuidv4 = require('uuid/v4'),
    Schema = mongoose.Schema,
    bearerRegex = /^Bearer (\S+)$/;

/**
 * Service secret key
 * @type {{username: string, password: string}}
 */
const secret = {secret: '1ddf67f5-44e4-4392-96e7-99a0fd882349'};

/**
 * Validates input secret with above stored secret
 * @param input
 * @returns {boolean}
 */
function validateCredentials(input) {
    return input.secret === secret.secret;
}

/**
 * Token schema and model definition
 */
const Token = mongoose.model('Token', new Schema({
    token: {type: String},
    token_type: {type: String},
    expires_in: {type: Number},
    expires_at: {type: Number}
}));

/**
 * Creates access token and saves it to mongodb
 * @returns {Promise}
 */
function createToken() {
    const now = new Date();
    const token = new Token({
        token: uuidv4(),
        token_type: 'bearer',
        expires_in: 3600,
        expires_at: Math.floor(now.setHours(now.getHours() + 1) / 1000)
    });

    return new Promise((resolve, reject) => {
        token.save((err, data) => {
            if (err) reject(err);
            else resolve(data);
        });
    });
}

/**
 * Creates, saves, and returns a bearer token
 * Exposed through HTTP API interface
 * @param req
 * @param res
 * @returns {*}
 */
let returnToken = (req, res) => {
    if (!validateCredentials(req.body)) return res.status(403).json({error: 'forbidden'});

    createToken()
        .then((body) => {
            console.log('\ntoken created:\n', body);
            return res.status(201).json(body);
        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({error: 'token creation failed'});
        })
};

/**
 * Verifies all requests for a valid bearer token
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
let authorize = (req, res, next) => {
    if (!req.get('authorization')) return res.status(403).json({error: 'forbidden'});
    const bearer = bearerRegex.exec(req.get('authorization'))[1];
    const now = Math.floor(Date.now() / 1000);

    Token.where({token: bearer}).findOne((err, body) => {
        if (err || !body) {
            console.error('auth failure for token:\n', bearer, '\nerror:\n', err || 'token not found');
            return res.status(403).json({error: 'forbidden'});
        } else if (now > body.expires_at) {
            console.error('auth failure for token:\n', body, '\nerror:\n', err || 'token expired');
            return res.status(403).json({error: 'forbidden'});
        }

        console.log('token validated:\n', body);
        next();
    });
};

module.exports = {
    authorize,
    returnToken,
};
